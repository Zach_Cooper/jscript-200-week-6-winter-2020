
$(document).ready(function() {
/**
 * * Toggles "done" class on <li> element
 */
// Trigger the event on the li element
$('li').on('click', function() {
  $(this).attr('class', 'done');
});

 /** 
 * Delete element when delete link clicked
 */
 // $('.delete').on('click', function() {
 //   $(this).parent().remove();
 //});
 // Delete element with fadeout
 $('.delete').on('click', function() {
  $(this).fadeOut( 800, function() {
    $(this).parent().remove();
  })
});

/**
 * Adds new list item to <ul>
 */
const addListItem = function () {
  const text = $('input').val();

  //Span Element Part
  const $spanElement = $('<span>');
  $spanElement.text(text);

  //Delete Element Part
  const $deleteElement = $('<a>');
  $deleteElement.text('Delete');
  $deleteElement.attr('class', 'delete');

  //Create Element Item Part
  const $liElement = $('<li>');
  $liElement.append($spanElement);
  $liElement.append($deleteElement);
  $('ul').append($liElement);

  //Add listeners "done" & "delete"
  //.attr: 1 argument accesses, 2 arguments change
  $('li').on('click', function() {
    $(this).attr('class', 'done');
  });

  $('.delete').on('click', function() {
    $(this).fadeOut( 800, function() {
      $(this).parent().remove();
    });
  });

};

// add listener for add
$('.add-item').on('click', function () {
  addListItem();
});

});

//**Tried to do it in one shot!! */
//const addListItem = function(e) {
  //e.preventDefault();
//  const text = $('input').val();
  // Append down the DOM
//  $('.today-list').append(
//    $('<li>').append(
//      $('<span>').append(text),
//      //.attr: 1 argument accesses, 2 arguments change
//      $('<a>').attr('class', 'delete').append('Delete')
//    )
//  )

